/*
 * This file is part of ClassicMod, licensed under the MIT License (MIT).
 *
 * Copyright (c) 2015, Jamie Mansfield <https://github.com/jamierocks>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.mojang.minecraft;

import uk.jamierocks.classicmod.launch.ClassicModTweaker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public final class ResourceDownlaodThread extends Thread {

    private static final String[] resourceFiles = new String[]{"music/calm1.ogg",
            "music/calm2.ogg", "music/calm3.ogg", "newmusic/hal1.ogg", "newmusic/hal2.ogg",
            "newmusic/hal3.ogg", "newmusic/hal4.ogg", "newsound/step/grass1.ogg",
            "newsound/step/grass2.ogg", "newsound/step/grass3.ogg", "newsound/step/grass4.ogg",
            "newsound/step/gravel1.ogg", "newsound/step/gravel2.ogg", "newsound/step/gravel3.ogg",
            "newsound/step/gravel4.ogg", "newsound/step/stone1.ogg", "newsound/step/stone2.ogg",
            "newsound/step/stone3.ogg", "newsound/step/stone4.ogg", "newsound/step/wood1.ogg",
            "newsound/step/wood2.ogg", "newsound/step/wood3.ogg", "newsound/step/wood4.ogg",
            "newsound/step/cloth1.ogg", "newsound/step/cloth2.ogg", "newsound/step/cloth3.ogg",
            "newsound/step/cloth4.ogg", "newsound/step/sand1.ogg", "newsound/step/sand2.ogg",
            "newsound/step/sand3.ogg", "newsound/step/sand4.ogg", "newsound/step/snow1.ogg",
            "newsound/step/snow2.ogg", "newsound/step/snow3.ogg", "newsound/step/snow4.ogg",
            "sound3/dig/grass1.ogg", "sound3/dig/grass2.ogg", "sound3/dig/grass3.ogg",
            "sound3/dig/grass4.ogg", "sound3/dig/gravel1.ogg", "sound3/dig/gravel2.ogg",
            "sound3/dig/gravel3.ogg", "sound3/dig/gravel4.ogg", "sound3/dig/stone1.ogg",
            "sound3/dig/stone2.ogg", "sound3/dig/stone3.ogg", "sound3/dig/stone4.ogg",
            "sound3/dig/wood1.ogg", "sound3/dig/wood2.ogg", "sound3/dig/wood3.ogg",
            "sound3/dig/wood4.ogg", "sound3/dig/cloth1.ogg", "sound3/dig/cloth2.ogg",
            "sound3/dig/cloth3.ogg", "sound3/dig/cloth4.ogg", "sound3/dig/sand1.ogg",
            "sound3/dig/sand2.ogg", "sound3/dig/sand3.ogg", "sound3/dig/sand4.ogg",
            "sound3/dig/snow1.ogg", "sound3/dig/snow2.ogg", "sound3/dig/snow3.ogg",
            "sound3/dig/snow4.ogg", "sound3/random/glass1.ogg", "sound3/random/glass2.ogg",
            "sound3/random/glass3.ogg"};
    public static boolean done = false;

    private File dir;
    private Minecraft minecraft;
    boolean running = false;
    private boolean finished = false;

    public ResourceDownlaodThread(File minecraftFolder, Minecraft minecraft) {
        this.minecraft = minecraft;

        this.setName("Resource download thread");
        this.setDaemon(true);

        this.dir = new File(minecraftFolder, "resources/");

        if (!this.dir.exists() && !this.dir.mkdirs()) {
            throw new RuntimeException("The working directory could not be created: " + this.dir);
        }
    }

    public final void run() {
        File musicFolder = new File(dir, "music");
        File stepsFolder = new File(new File(dir, "newsound"), "step");
        File digFolder = new File(new File(dir, "sound3"), "dig");
        File randomFolder = new File(new File(dir, "sound3"), "random");
        File newMusicFolder = new File(dir, "newmusic");

        try {
            ClassicModTweaker.getLogger().info("Downloading music and sounds...");

            int percent = 5;
            for (String fileName : resourceFiles) {
                if (percent >= 80) {
                    percent = 80;
                }
                percent += 3;
                File file = new File(dir, fileName);
                if (!file.exists()) {
                    ClassicModTweaker.getLogger().info("Downloading https://s3.amazonaws.com/MinecraftResources/"
                            + fileName);
                    URL url = new URL("https://s3.amazonaws.com/MinecraftResources/" + fileName);
                    try (InputStream is = url.openStream()) {
                        copyStreamToFile(is, file);
                    }

                    ClassicModTweaker.getLogger().info("Downloaded https://s3.amazonaws.com/MinecraftResources/"
                            + fileName);
                }
            }
            ClassicModTweaker.getLogger().info("Done downloading music and sounds!");
            done = true;
        } catch (Exception ex) {
            ClassicModTweaker.getLogger().warn("Error downloading music and sounds!", ex);
        }

        for (int i = 1; i <= 3; i++) {
            minecraft.sound.registerMusic("calm" + i + ".ogg", new File(musicFolder, "calm" + i
                    + ".ogg"));
            minecraft.sound.registerSound(new File(randomFolder, "glass" + i + ".ogg"),
                    "random/glass" + i + ".ogg");
        }

        for (int i = 1; i <= 4; i++) {
            minecraft.sound.registerMusic("calm" + i + ".ogg", new File(newMusicFolder, "hal" + i
                    + ".ogg"));
            minecraft.sound.registerSound(new File(stepsFolder, "grass" + i + ".ogg"), "step/grass"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "gravel" + i + ".ogg"),
                    "step/gravel" + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "stone" + i + ".ogg"), "step/stone"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "wood" + i + ".ogg"), "step/wood"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "cloth" + i + ".ogg"), "step/cloth"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "sand" + i + ".ogg"), "step/sand"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(stepsFolder, "snow" + i + ".ogg"), "step/snow"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "grass" + i + ".ogg"), "dig/grass"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "gravel" + i + ".ogg"), "dig/gravel"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "stone" + i + ".ogg"), "dig/stone"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "wood" + i + ".ogg"), "dig/wood" + i
                    + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "cloth" + i + ".ogg"), "dig/cloth"
                    + i + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "sand" + i + ".ogg"), "dig/sand" + i
                    + ".ogg");
            minecraft.sound.registerSound(new File(digFolder, "snow" + i + ".ogg"), "dig/snow" + i
                    + ".ogg");
        }

        finished = true;
    }

    private static final int BUFFER_SIZE = 64 * 1024; // 64 KB

    public static void copyStreamToFile(InputStream inStream, File file) throws IOException {
        try (ReadableByteChannel in = Channels.newChannel(inStream)) {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            try (FileOutputStream outStream = new FileOutputStream(file)) {
                FileChannel out = outStream.getChannel();
                long offset = 0;
                long count;
                while ((count = out.transferFrom(in, offset, BUFFER_SIZE)) > 0) {
                    offset += count;
                }
            }
        }
    }
}
