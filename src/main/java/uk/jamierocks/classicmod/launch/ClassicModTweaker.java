/*
 * This file is part of ClassicMod, licensed under the MIT License (MIT).
 *
 * Copyright (c) 2015, Jamie Mansfield <https://github.com/jamierocks>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.jamierocks.classicmod.launch;

import static com.google.common.io.Resources.getResource;

import com.mojang.minecraft.Minecraft;
import net.minecraft.launchwrapper.ITweaker;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.launchwrapper.LaunchClassLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.LWJGLUtil;
import uk.jamierocks.classicmod.util.LWJGLNatives;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ClassicModTweaker implements ITweaker {

    private static final Logger logger = LogManager.getLogger("ClassicMod");

    private List<String> args;

    public static Logger getLogger() {
        return logger;
    }

    @Override
    public void acceptOptions(List<String> args, File gameDir, File assetsDir, String profile) {
        this.args = args;
    }

    @Override
    public void injectIntoClassLoader(LaunchClassLoader classLoader) {
        logger.info("Loading ClassicMod...");

        // classicmod launch
        classLoader.addClassLoaderExclusion("uk.jamierocks.classicmod.launch.");

        logger.debug("Applying runtime de-obfuscation...");
        if (isObfuscated()) {
            logger.info("De-obfuscation mappings are provided by Jamie Mansfield");
            Launch.blackboard.put("classic.mappings", getResource("mappings.srg"));
            classLoader.registerTransformer("uk.jamierocks.classicmod.launch.transformer.DeobfuscationTransformer");
            logger.debug("Runtime de-obfuscation is applied.");
        } else {
            logger.debug("Runtime de-obfuscation was not applied. ClassicMod is being loaded in a de-obfuscated environment.");
        }
        LWJGLNatives.load(new File("lib"));

        classLoader.registerTransformer("net.minecraft.launchwrapper.injector.AlphaVanillaTweakInjector");
    }

    @Override
    public String getLaunchTarget() {
        return "net.minecraft.launchwrapper.injector.AlphaVanillaTweakInjector";
    }

    @Override
    public String[] getLaunchArguments() {
        return args.toArray(new String[args.size()]);
    }

    private static boolean isObfuscated() {
        try {
            return Launch.classLoader.getClassBytes("com.mojang.minecraft.level.Level") == null;
        } catch (IOException ignored) {
            return true;
        }
    }
}
