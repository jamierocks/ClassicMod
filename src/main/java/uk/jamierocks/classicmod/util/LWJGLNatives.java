/*
 * This file is part of ClassicMod, licensed under the MIT License (MIT).
 *
 * Copyright (c) 2015, Jamie Mansfield <https://github.com/jamierocks>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.jamierocks.classicmod.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.lwjgl.LWJGLUtil;
import uk.jamierocks.classicmod.launch.ClassicModMain;
import uk.jamierocks.classicmod.launch.ClassicModTweaker;

public class LWJGLNatives {

    public static void load(File dir) {
        if(LWJGLUtil.getPlatform() == LWJGLUtil.PLATFORM_WINDOWS) {
            extract(dir, "lwjgl.dll", "86");
            extract(dir, "lwjgl64.dll", "64");
            extract(dir, "OpenAL32.dll", "86");
            extract(dir, "OpenAL64.dll", "64");
            extract(dir, "jinput-raw.dll", "86");
            extract(dir, "jinput-raw_64.dll", "64");
            extract(dir, "jinput-dx8.dll", "86");
            extract(dir, "jinput-dx8_64.dll", "64");
        } else if(LWJGLUtil.getPlatform() == LWJGLUtil.PLATFORM_LINUX) {
            extract(dir, "liblwjgl.so", "86");
            extract(dir, "liblwjgl64.so", "64");
            extract(dir, "libopenal.so", "86");
            extract(dir, "libopenal64.so", "64");
            extract(dir, "libjinput-linux.so", "86");
            extract(dir, "libjinput-linux64.so", "64");
        } else if(LWJGLUtil.getPlatform() == LWJGLUtil.PLATFORM_MACOSX) {
            extract(dir, "liblwjgl.dylib", "both");
            extract(dir, "openal.dylib", "both");
            extract(dir, "libjinput-osx.jnilib", "both");
        } else {
            throw new RuntimeException("nope");
        }

        System.setProperty("java.library.path", dir.getAbsolutePath());
        System.setProperty("org.lwjgl.librarypath", dir.getAbsolutePath());
        System.setProperty("net.java.games.input.librarypath", dir.getAbsolutePath());
    }

    private static void extract(File dir, String lib, String arch) {
        if(arch.equals("both") || System.getProperty("os.arch").contains(arch)) {
            File file = new File(dir, lib);
            boolean writing = false;
            ClassicModTweaker.getLogger().info("Checking for " + lib + " at " + file.getPath());
            try {
                if(file.exists()) {
                    InputStream in = ClassicModMain.class.getResourceAsStream("/" + lib);
                    InputStream fin = new FileInputStream(file);
                    try {
                        if(IOUtils.contentEquals(in, fin)) {
                            ClassicModTweaker.getLogger().info(lib + " is up to date at " + file.getPath());
                            return;
                        }
                    } finally {
                        IOUtils.closeQuietly(in);
                        IOUtils.closeQuietly(fin);
                    }
                }

                writing = true;
                InputStream in = ClassicModMain.class.getResourceAsStream("/" + lib);
                ClassicModTweaker.getLogger().info("Writing " + lib + " to " + file.getPath());
                FileOutputStream out = new FileOutputStream(file);
                IOUtils.copy(in, out);
                IOUtils.closeQuietly(in);
                IOUtils.closeQuietly(out);
            } catch(Exception e) {
                if(writing) {
                    ClassicModTweaker.getLogger().warn("Failed to write library " + lib + " to " + file.getPath());
                } else {
                    ClassicModTweaker.getLogger().warn("Failed to check for library " + lib + " at " + file.getPath());
                }

                e.printStackTrace();
            }
        }
    }

}